package com.liveandcook.mobile.beans;

/**
 * Created by Android1 on 9/2/2015.
 */
public class RecipeWoIngredients {

    public String recipeId;
    public String title;
    public String imageUrl;
    public String sourceUrl;
    public String socialRank;

    public RecipeWoIngredients(String recipeId, String title, String imageUrl, String sourceUrl, String socialRank) {
        this.recipeId = recipeId;
        this.title = title;
        this.imageUrl = imageUrl;
        this.sourceUrl = sourceUrl;
        this.socialRank = socialRank;
    }

    public RecipeWoIngredients(String imageUrl, String title) {
        this.imageUrl = imageUrl;
        this.title = title;
    }

    @Override
    public String toString(){
        return "{recipeId:" + recipeId + "," +
                "title:" + title + "," +
                "imageUrl:" + imageUrl + "," +
                "sourceUrl:" + sourceUrl + "," +
                "socialRank:" + socialRank +
                "}";
    }
}
