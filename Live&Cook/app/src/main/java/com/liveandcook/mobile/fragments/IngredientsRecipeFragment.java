package com.liveandcook.mobile.fragments;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.liveandcook.mobile.R;
import com.liveandcook.mobile.adapters.RecipeImageAdapter;
import com.liveandcook.mobile.adapters.RecipeIngredientAdapter;
import com.liveandcook.mobile.beans.RecipeWoIngredients;
import com.liveandcook.mobile.interfaces.SuggestionCommunicatorInterface;
import com.liveandcook.mobile.mvc.controllers.RecipeSuggestionController;
import com.liveandcook.mobile.util.Food2ForkAPIConstants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Android1 on 8/31/2015.
 */
public class IngredientsRecipeFragment extends Fragment {

    private RecipeSuggestionController recipeSuggestionController;
    private SuggestionCommunicatorInterface communicatorInterface;

    public static final String DATASET_CHANGES_INTENT = "com.liveandcook.mobile.fragments.IngredientRecipeBroadcastReceiver.notifyDataSetChanges";
    private final IntentFilter intentFilter = new IntentFilter(DATASET_CHANGES_INTENT);
    private final IngredientRecipeBroadcastReceiver receiver = new IngredientRecipeBroadcastReceiver();
    private LocalBroadcastManager mBroadcastMgr;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecipeIngredientAdapter adapter;

    public IngredientsRecipeFragment(){

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        communicatorInterface = (SuggestionCommunicatorInterface) activity;
        recipeSuggestionController = ((RecipeSuggestionFragment)getParentFragment()).getRecipeSuggestionController();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(IngredientsRecipeFragment.class.getSimpleName(), "onCreateView");

        //---Inflate the layout for this fragment---
        View view = inflater.inflate(R.layout.f_ingredients_recipe, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.f_ing_rv_recipe_ingredients);
        recyclerView.setHasFixedSize(true);

        //layoutManager = new GridLayoutManager(getActivity(),1);
        layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapter = new RecipeIngredientAdapter(recipeSuggestionController.getRecipeSuggestionModel().recipeSelected.ingredientList);
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        mBroadcastMgr = LocalBroadcastManager.getInstance(getActivity().getApplicationContext());
        mBroadcastMgr.registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        mBroadcastMgr.unregisterReceiver(receiver);
        super.onPause();
    }

    public void showIngredients(String recipeId){
        Toast.makeText(getActivity(), "Data sent from ImageRecipeFragment: " + recipeId, Toast.LENGTH_SHORT).show();

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put(Food2ForkAPIConstants.PARAMETER_KEY, Food2ForkAPIConstants.VALUE_API_KEY);
        parameters.put(Food2ForkAPIConstants.PARAMETER_RID, recipeId);

        recipeSuggestionController.executeGetRecipeRequest(parameters);
    }

    public class IngredientRecipeBroadcastReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(IngredientRecipeBroadcastReceiver.class.getSimpleName(), "onReceive");
            adapter.setIngredientList(recipeSuggestionController.getRecipeSuggestionModel().recipeSelected.ingredientList);
            adapter.notifyDataSetChanged();
        }
    }

}
