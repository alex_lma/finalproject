package com.liveandcook.mobile.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liveandcook.mobile.R;
import com.liveandcook.mobile.mvc.controllers.RecipeSuggestionController;
import com.liveandcook.mobile.mvc.models.RecipeSuggestionModel;

/**
 * Created by Android1 on 8/31/2015.
 */
public class RecipeSuggestionFragment extends Fragment {

    private RecipeSuggestionController recipeSuggestionController;
    private RecipeSuggestionModel recipeSuggestionModel;

    public RecipeSuggestionFragment(){

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        recipeSuggestionModel = new RecipeSuggestionModel();
        recipeSuggestionController = new RecipeSuggestionController(recipeSuggestionModel, getActivity());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(RecipeSuggestionFragment.class.getSimpleName(), "onCreateView");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.f_recipe_suggestion, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public RecipeSuggestionController getRecipeSuggestionController() {
        return recipeSuggestionController;
    }

    public void setRecipeSuggestionController(RecipeSuggestionController recipeSuggestionController) {
        this.recipeSuggestionController = recipeSuggestionController;
    }

    public RecipeSuggestionModel getRecipeSuggestionModel() {
        return recipeSuggestionModel;
    }

    public void setRecipeSuggestionModel(RecipeSuggestionModel recipeSuggestionModel) {
        this.recipeSuggestionModel = recipeSuggestionModel;
    }
}
