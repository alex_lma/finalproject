package com.liveandcook.mobile.mvc.controllers;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.liveandcook.mobile.R;
import com.liveandcook.mobile.activities.RecipeMainActivity;
import com.liveandcook.mobile.fragments.ImageRecipeFragment;
import com.liveandcook.mobile.fragments.IngredientsRecipeFragment;
import com.liveandcook.mobile.fragments.RecipeSuggestionFragment;
import com.liveandcook.mobile.mvc.models.RecipeSuggestionModel;
import com.liveandcook.mobile.util.Food2ForkAPIConstants;
import com.liveandcook.mobile.util.RecipeJsonObjectRequest;
import com.liveandcook.mobile.util.RequestQueueSingleton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Android1 on 9/2/2015.
 */
public class RecipeSuggestionController {

    private RecipeSuggestionModel recipeSuggestionModel;
    private Context context;
    private LocalBroadcastManager mBroadcastMgr;

    public RecipeSuggestionController(RecipeSuggestionModel recipeSuggestionModel, Context context){
        this.recipeSuggestionModel = recipeSuggestionModel;
        this.context = context;
        mBroadcastMgr = LocalBroadcastManager.getInstance(context);
    }

    public synchronized void executeSearchRecipesRequest(Map<String, String> parameters){

        RecipeJsonObjectRequest recipeJsonObjReq = new RecipeJsonObjectRequest(
                Food2ForkAPIConstants.URL_SEARCH_RECIPES,
                new SearchRecipeResponseListener(),
                new RecipeErrorListener()
        );

        Map<String, String> headers = new HashMap<String, String>();
        headers.put(Food2ForkAPIConstants.HEADER_X_MASHAPE_KEY, Food2ForkAPIConstants.VALUE_X_MASHAPE_KEY);

        recipeJsonObjReq.setHeaders(headers);
        recipeJsonObjReq.setParameters(parameters);

        RequestQueueSingleton.getInstance().addToRequestQueue(recipeJsonObjReq, RecipeJsonObjectRequest.SEARCH_REQUEST, context);
    }

    public synchronized void executeGetRecipeRequest(Map<String, String> parameters){

        RecipeJsonObjectRequest recipeJsonObjReq = new RecipeJsonObjectRequest(
                Food2ForkAPIConstants.URL_GET_RECIPE,
                new GetRecipeResponseListener(),
                new RecipeErrorListener()
        );

        Map<String, String> headers = new HashMap<String, String>();
        headers.put(Food2ForkAPIConstants.HEADER_X_MASHAPE_KEY, Food2ForkAPIConstants.VALUE_X_MASHAPE_KEY);

        recipeJsonObjReq.setHeaders(headers);
        recipeJsonObjReq.setParameters(parameters);

        RequestQueueSingleton.getInstance().addToRequestQueue(recipeJsonObjReq, RecipeJsonObjectRequest.GET_REQUEST, context);
    }

    public class GetRecipeResponseListener implements Response.Listener<String>{

        @Override
        public void onResponse(String response) {
            Log.d(RecipeMainController.class.getSimpleName(), "RESPONSE: " + response.toString());
            recipeSuggestionModel.setRecipeSelected(RecipeSuggestionModel.parseGetJsonObjToRecipeObj(response));

            mBroadcastMgr.sendBroadcast(new Intent(IngredientsRecipeFragment.DATASET_CHANGES_INTENT));
        }
    }

    public class SearchRecipeResponseListener implements Response.Listener<String>{

        @Override
        public void onResponse(String response) {
            Log.d(RecipeMainController.class.getSimpleName(), "RESPONSE: " + response.toString());
            recipeSuggestionModel.setRecipeSearch(RecipeSuggestionModel.parseSearchJsonObjToRecipeObj(response));

            mBroadcastMgr.sendBroadcast(new Intent(ImageRecipeFragment.DATASET_CHANGES_INTENT));

            //context.sendBroadcast();

            /*
            ImageRecipeFragment imageRecipeFragment1 = null;

            imageRecipeFragment1 = (ImageRecipeFragment) recipeSuggestionFragment.getChildFragmentManager().findFragmentById(R.id.recipe_image_frame);
            Log.d(RecipeSuggestionController.class.getSimpleName(), "imageRecipeFragment1: " + imageRecipeFragment1);

            imageRecipeFragment1.notifyRecyclerViewDataChanges();
            */
        }
    }

    public class RecipeErrorListener implements Response.ErrorListener{

        @Override
        public void onErrorResponse(VolleyError error) {
            Log.d("ErrorSearch", "ERROR ");

            String json = null;

            NetworkResponse response = error.networkResponse;
            if(response != null && response.data != null){
                switch(response.statusCode){
                    case 400:
                        json = new String(response.data);
                        json = trimMessage(json, "message");
                        if(json != null)
                        {
                            Log.d("ErrorSearch", json);
                            Toast.makeText(context, json, Toast.LENGTH_LONG).show();
                        }
                        break;
                }
                //Additional cases
            }
        }

        public String trimMessage(String msg, String key){
            String trimmedMsg = null;

            try{
                JSONObject obj = new JSONObject(msg);
                trimmedMsg = obj.getString(key);
            } catch(JSONException e){
                e.printStackTrace();
                return null;
            }

            return trimmedMsg;
        }
    }

    public class OnRecipeImgCVTouchListener implements View.OnClickListener{
        public int recipeId;

        OnRecipeImgCVTouchListener(int recipeId){
            this.recipeId = recipeId;
        }

        @Override
        public void onClick(View v) {
            HashMap<String, String> parameters = new HashMap<>();
            parameters.put(Food2ForkAPIConstants.PARAMETER_RID, String.valueOf(recipeId));
            Log.d("On click Image Recipe", "Parameters: " +
                    Food2ForkAPIConstants.PARAMETER_RID + ":" + parameters.get(Food2ForkAPIConstants.PARAMETER_RID));

            executeGetRecipeRequest(parameters);
        }
    }

    public RecipeSuggestionModel getRecipeSuggestionModel() {
        return recipeSuggestionModel;
    }

    public void setRecipeSuggestionModel(RecipeSuggestionModel recipeSuggestionModel) {
        this.recipeSuggestionModel = recipeSuggestionModel;
    }
}
