package com.liveandcook.mobile.util;

/**
 * Created by Android1 on 9/1/2015.
 */
public class Food2ForkAPIConstants {

    /** URLs */
    /** key and q parameters are required with SEARCH URL; sort and page parameters are optional
     * Any request will return a maximum of 30 results. To get the next set of results
     * send the same request again but with page = 2.
     * The default omitted is page = 1.
     **/
    public static String URL_SEARCH_RECIPES = "https://community-food2fork.p.mashape.com/search";
    /** key and rId (recipe Id) parameter are required with GET URL */
    public static String URL_GET_RECIPE = "https://community-food2fork.p.mashape.com/get";


    /** KEYS */
    public static String VALUE_API_KEY = "1f88c03ef73abe2dd9b033faa13a2cd3";
    public static String VALUE_X_MASHAPE_KEY = "ukikgcjFkSmsh1L93Esb9oYuFLDTp1taDwcjsnLtBoXqh7JoSC";


    /** SEARCH URL PARAMETERS */
    public static String PARAMETER_KEY = "key";
    public static String PARAMETER_Q = "q";
    public static String PARAMETER_SORT = "sort";
    public static String PARAMETER_PAGE = "page";


    /** SEARCH URL PARAMETER VALUES */
    public static String VALUE_SORT_BY_RATING = "r";
    public static String VALUE_SORT_BY_TRENDINGNESS = "t";


    /** GET PARAMETERS */
    /** Recipe Id */
    public static String PARAMETER_RID = "rId";

    /** GET & SEARCH HEADER TAGS */
    public static String HEADER_X_MASHAPE_KEY = "X-Mashape-Key";


    /** JSON ITEM TAGS */
    public static String TAG_COUNT = "count";
    public static String TAG_RECIPES = "recipes";
    public static String TAG_RECIPE_ID = "recipe_id";

    public static String TAG_RECIPE = "recipe";
    public static String TAG_TITLE = "title";
    public static String TAG_INGREDIENTS = "ingredients";
    public static String TAG_IMAGE_URL = "image_url";
    public static String TAG_SOURCE_URL = "source_url";
    public static String TAG_SOCIAL_RANK = "social_rank";

}
