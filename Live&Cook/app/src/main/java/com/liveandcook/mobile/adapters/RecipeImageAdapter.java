package com.liveandcook.mobile.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.liveandcook.mobile.R;
import com.liveandcook.mobile.activities.RecipeMainActivity;
import com.liveandcook.mobile.beans.Recipe;
import com.liveandcook.mobile.beans.RecipeWoIngredients;
import com.liveandcook.mobile.fragments.ImageRecipeFragment;
import com.liveandcook.mobile.mvc.controllers.RecipeSuggestionController;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Android1 on 9/1/2015.
 */
public class RecipeImageAdapter extends RecyclerView.Adapter<RecipeImageAdapter.RecipeImageViewHolder>
{
    private static ImageRecipeFragment imageRecipeFragment;
    private Context context;
    private ArrayList<RecipeWoIngredients> recipeDataSet;


    public RecipeImageAdapter(Context context, ImageRecipeFragment imageRecipeFragment, ArrayList<RecipeWoIngredients> recipeDataSet){
        this.context = context;
        this.imageRecipeFragment = imageRecipeFragment;
        this.recipeDataSet = recipeDataSet;
    }


    public ArrayList<RecipeWoIngredients> getRecipeDataSet() {
        return recipeDataSet;
    }

    public void setRecipeDataSet(ArrayList<RecipeWoIngredients> recipeDataSet) {
        this.recipeDataSet = recipeDataSet;
    }

    @Override
    public RecipeImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cv_recipe_image, parent, false);
        RecipeImageViewHolder recipeImageViewHolder = new RecipeImageViewHolder(view);

        return recipeImageViewHolder;
    }

    @Override
    public void onBindViewHolder(RecipeImageViewHolder viewHolder, int position) {
        RecipeWoIngredients recipeModel = recipeDataSet.get(position);

        viewHolder.recipeId = recipeModel.recipeId;

        Picasso.with(context).setDebugging(true);
        Picasso.with(context)
                .load(recipeModel.imageUrl)
                .resize(500, 700).centerCrop()
                .into(viewHolder.recipeImage);

        viewHolder.title.setText(recipeModel.title);

        Log.d(RecipeImageViewHolder.class.getSimpleName(), "Title: " + recipeModel.title + " imageUrl: " + recipeModel.imageUrl);

    }

    @Override
    public int getItemCount() {
        return recipeDataSet.size();
    }

    public static class RecipeImageViewHolder extends RecyclerView.ViewHolder{
        String recipeId;
        ImageView recipeImage;
        TextView title;

        public RecipeImageViewHolder(View itemView){
            super(itemView);

            this.recipeImage = (ImageView) itemView.findViewById(R.id.cv_img_recipe);
            this.title = (TextView) itemView.findViewById(R.id.cv_tv_recipe_title);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageRecipeFragment.sendRecipeToShowIngredients(recipeId);
                }
            });
        }
    }

}
