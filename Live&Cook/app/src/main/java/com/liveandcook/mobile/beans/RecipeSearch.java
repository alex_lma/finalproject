package com.liveandcook.mobile.beans;

import java.util.ArrayList;

/**
 * Created by Android1 on 9/1/2015.
 */
public class RecipeSearch {

    public String query;
    public String count;
    public String page;
    public ArrayList<RecipeWoIngredients> recipeList;

    public RecipeSearch(){
        query = "";
        count = "0";
        page = "1";
        recipeList = new ArrayList<RecipeWoIngredients>();
    }

    public RecipeSearch(String query, String page) {
        this.query = query;
        this.page = page;
    }

    public RecipeSearch(String count, ArrayList<RecipeWoIngredients> recipeList) {
        this.count = count;
        this.recipeList = recipeList;
    }

    @Override
    public String toString(){

        String recipeListStr = "";
        int lon = recipeList.size();

        for(int i=0; i<lon; i++) {

            if(i < (lon-1)) {
                recipeListStr += recipeList.get(i) + ",";
            }
        }

        recipeListStr += recipeList.get(lon-1);

        return "{query:" + query + "," +
                "count:" + count + "," +
                "page:" + page + "," +
                "ingredients:[" + recipeListStr + "]" +
                "}";
    }
}
