package com.liveandcook.mobile.util;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by Android1 on 9/2/2015.
 */
public class RecipeJsonObjectRequest extends StringRequest {

    public static String SEARCH_REQUEST = "SEARCH_REQUEST";
    public static String GET_REQUEST = "GET_REQUEST";

    private Map<String, String> parameters;
    private Map<String, String> headers;

    public RecipeJsonObjectRequest(String url, Response.Listener<String> responseListener,
                                   Response.ErrorListener errorListener){
        super(Method.POST, url, responseListener, errorListener);
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        Log.d(RecipeJsonObjectRequest.class.getSimpleName(), "Parameters: " + parameters);
        return parameters;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Log.d(RecipeJsonObjectRequest.class.getSimpleName(), "Headers: " + headers);
        return headers;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
}
