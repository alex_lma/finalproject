package com.liveandcook.mobile.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.liveandcook.mobile.R;
import com.liveandcook.mobile.adapters.RecipeImageAdapter;
import com.liveandcook.mobile.beans.Recipe;
import com.liveandcook.mobile.beans.RecipeWoIngredients;
import com.liveandcook.mobile.interfaces.SuggestionCommunicatorInterface;
import com.liveandcook.mobile.mvc.controllers.RecipeMainController;
import com.liveandcook.mobile.mvc.controllers.RecipeSuggestionController;
import com.liveandcook.mobile.mvc.models.RecipeSuggestionModel;
import com.liveandcook.mobile.util.Food2ForkAPIConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Android1 on 8/31/2015.
 */
public class ImageRecipeFragment extends Fragment {

    private RecipeSuggestionController recipeSuggestionController;
    private SuggestionCommunicatorInterface suggestionCommunicatorInterface;

    public static final String DATASET_CHANGES_INTENT = "com.liveandcook.mobile.fragments.ImageRecipeBroacastReciver.notifyDataSetChanges";
    private final IntentFilter intentFilter = new IntentFilter(DATASET_CHANGES_INTENT);
    private final ImageRecipeBroadcastReciver receiver = new ImageRecipeBroadcastReciver();
    private LocalBroadcastManager mBroadcastMgr;

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private RecipeImageAdapter adapter;

    public ImageRecipeFragment(){

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        suggestionCommunicatorInterface = (SuggestionCommunicatorInterface)activity;
        recipeSuggestionController = ((RecipeSuggestionFragment)getParentFragment()).getRecipeSuggestionController();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d(ImageRecipeFragment.class.getSimpleName(), "onCreateView");

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.f_image_recipe, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.f_img_rv_recipe_image);
        recyclerView.setHasFixedSize(true);

        //layoutManager = new GridLayoutManager(getActivity(),1);
        layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapter = new RecipeImageAdapter(getActivity(), this, recipeSuggestionController.getRecipeSuggestionModel().recipeSearch.recipeList);
        recyclerView.setAdapter(adapter);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put(Food2ForkAPIConstants.PARAMETER_KEY, Food2ForkAPIConstants.VALUE_API_KEY);
        parameters.put(Food2ForkAPIConstants.PARAMETER_Q, "chicken");
        //parameters.put(Food2ForkAPIConstants.PARAMETER_PAGE, "2");
        //parameters.put(Food2ForkAPIConstants.PARAMETER_SORT, Food2ForkAPIConstants.VALUE_SORT_BY_RATING);
        //parameters.put(Food2ForkAPIConstants.PARAMETER_SORT, Food2ForkAPIConstants.VALUE_SORT_BY_TRENDINGNESS);

        /*ArrayList<RecipeWoIngredients> recipeList = new ArrayList<RecipeWoIngredients>();

        recipeList.add(new RecipeWoIngredients("http://static.food2fork.com/Bacon2BWrapped2BJalapeno2BPopper2BStuffed2BChicken2B5002B5909939b0e65.jpg", "one"));
        recipeList.add(new RecipeWoIngredients("http://static.food2fork.com/Bacon2BWrapped2BJalapeno2BPopper2BStuffed2BChicken2B5002B5909939b0e65.jpg", "two"));
        recipeList.add(new RecipeWoIngredients("http://static.food2fork.com/Bacon2BWrapped2BJalapeno2BPopper2BStuffed2BChicken2B5002B5909939b0e65.jpg", "three"));
        recipeList.add(new RecipeWoIngredients("http://static.food2fork.com/Bacon2BWrapped2BJalapeno2BPopper2BStuffed2BChicken2B5002B5909939b0e65.jpg", "four"));
        recipeList.add(new RecipeWoIngredients("http://static.food2fork.com/Bacon2BWrapped2BJalapeno2BPopper2BStuffed2BChicken2B5002B5909939b0e65.jpg", "five"));
        recipeList.add(new RecipeWoIngredients("http://static.food2fork.com/Bacon2BWrapped2BJalapeno2BPopper2BStuffed2BChicken2B5002B5909939b0e65.jpg", "six"));
        recipeList.add(new RecipeWoIngredients("http://static.food2fork.com/Bacon2BWrapped2BJalapeno2BPopper2BStuffed2BChicken2B5002B5909939b0e65.jpg", "seven"));
        recipeList.add(new RecipeWoIngredients("http://static.food2fork.com/Bacon2BWrapped2BJalapeno2BPopper2BStuffed2BChicken2B5002B5909939b0e65.jpg", "eight"));
        */
        recipeSuggestionController.executeSearchRecipesRequest(parameters);
    }

    @Override
    public void onResume() {
        super.onResume();
        //Register ImageRecipeBroacastReciver
        mBroadcastMgr = LocalBroadcastManager.getInstance(getActivity().getApplicationContext());
        mBroadcastMgr.registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onPause() {
        //Unregister ImageRecipeBroacastReciver
        mBroadcastMgr.unregisterReceiver(receiver);
        super.onPause();
    }

    public void sendRecipeToShowIngredients(String recipeId){
        suggestionCommunicatorInterface.updateRecipeIngredients(recipeId);
    }

    public class ImageRecipeBroadcastReciver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(ImageRecipeBroadcastReciver.class.getSimpleName(), "onReceive");
            adapter.setRecipeDataSet(recipeSuggestionController.getRecipeSuggestionModel().recipeSearch.recipeList);
            adapter.notifyDataSetChanged();
        }
    }
}
