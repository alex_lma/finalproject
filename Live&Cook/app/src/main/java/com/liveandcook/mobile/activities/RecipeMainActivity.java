package com.liveandcook.mobile.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.liveandcook.mobile.R;
import com.liveandcook.mobile.fragments.IngredientsRecipeFragment;
import com.liveandcook.mobile.fragments.RecipeSuggestionFragment;
import com.liveandcook.mobile.interfaces.SuggestionCommunicatorInterface;
import com.liveandcook.mobile.mvc.controllers.RecipeMainController;
import com.liveandcook.mobile.mvc.models.RecipeMainModel;

/**
 * Created by Android1 on 8/31/2015.
 */
public class RecipeMainActivity extends AppCompatActivity implements SuggestionCommunicatorInterface{

    public static String TAG_CLASS = RecipeMainActivity.class.getSimpleName();

    RecipeMainModel recipeMainModel;
    RecipeMainController recipeMainController;

    private Toolbar toolbar;
    private NavigationView navigationView;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.a_recipe_main_frame);

        recipeMainModel = new RecipeMainModel();
        recipeMainController = new RecipeMainController(recipeMainModel, this, getApplicationContext());

        initiateWidgets();
        loadRecipeSuggestionFragment();
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    public NavigationView getNavigationView() {
        return navigationView;
    }

    public void setNavigationView(NavigationView navigationView) {
        this.navigationView = navigationView;
    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }

    public void setDrawerLayout(DrawerLayout drawerLayout) {
        this.drawerLayout = drawerLayout;
    }

    public void initiateWidgets(){

        // Initializing Toolbar and setting it as the actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Initializing NavigationView
        navigationView = (NavigationView) findViewById(R.id.navigation_view);

        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(recipeMainController);

        // Initializing Drawer Layout and ActionBarToggle
        drawerLayout = (DrawerLayout) findViewById(R.id.a_login_drawer);

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.openDrawer, R.string.closeDrawer){

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                Toast.makeText(getApplicationContext(),"On Drawer Closed",Toast.LENGTH_SHORT).show();
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                Toast.makeText(getApplicationContext(),"On Drawer Opened",Toast.LENGTH_SHORT).show();
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawerLayout.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessay or else your the icons wont show up
        actionBarDrawerToggle.syncState();
    }

    public void loadRecipeSuggestionFragment(){
        RecipeSuggestionFragment fragment = new RecipeSuggestionFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.a_mainframe_wf, fragment);
        fragmentTransaction.commit();
    }

    public void displayMessage(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void updateRecipeIngredients(String recipeId) {

        RecipeSuggestionFragment iRecipeFrag = (RecipeSuggestionFragment) getSupportFragmentManager()
                .findFragmentById(R.id.a_mainframe_wf);
        IngredientsRecipeFragment ingredientsRecipeFragment = (IngredientsRecipeFragment) iRecipeFrag
                .getChildFragmentManager().findFragmentById(R.id.recipe_ingredients_frame);

        ingredientsRecipeFragment.showIngredients(recipeId);
    }
}
