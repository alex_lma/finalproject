package com.liveandcook.mobile.mvc.models;

import android.util.Log;

import com.liveandcook.mobile.beans.Ingredient;
import com.liveandcook.mobile.beans.Recipe;
import com.liveandcook.mobile.beans.RecipeSearch;
import com.liveandcook.mobile.beans.RecipeWoIngredients;
import com.liveandcook.mobile.mvc.controllers.RecipeMainController;
import com.liveandcook.mobile.util.Food2ForkAPIConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Android1 on 8/31/2015.
 */
public class RecipeSuggestionModel {

    public RecipeSearch recipeSearch;
    public Recipe recipeSelected;

    public RecipeSuggestionModel(){
        recipeSearch = new RecipeSearch();
        recipeSelected = new Recipe();
    }

    public RecipeSearch getRecipeSearch() {
        return recipeSearch;
    }

    public void setRecipeSearch(RecipeSearch recipeSearch) {
        this.recipeSearch = recipeSearch;
    }

    public Recipe getRecipeSelected() {
        return recipeSelected;
    }

    public void setRecipeSelected(Recipe recipeSelected) {
        this.recipeSelected = recipeSelected;
    }

    public static RecipeSearch parseSearchJsonObjToRecipeObj(String jsonStr){

        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(jsonStr);
        }catch(JSONException e){
            e.printStackTrace();
        }

        RecipeSearch recipeSearch = null;

        if (jsonStr != null) {

            try {

                JSONArray jsonRecipeArray = jsonObject.getJSONArray(Food2ForkAPIConstants.TAG_RECIPES);
                ArrayList<RecipeWoIngredients> recipeList = new ArrayList<RecipeWoIngredients>();

                for(int i=0; i<jsonRecipeArray.length(); i++) {

                    JSONObject jsonRecipeWoIngObj = (JSONObject) jsonRecipeArray.get(i);

                    RecipeWoIngredients recipeWoIngredients = new RecipeWoIngredients(
                            jsonRecipeWoIngObj.getString(Food2ForkAPIConstants.TAG_RECIPE_ID),
                            jsonRecipeWoIngObj.getString(Food2ForkAPIConstants.TAG_TITLE),
                            jsonRecipeWoIngObj.getString(Food2ForkAPIConstants.TAG_IMAGE_URL),
                            jsonRecipeWoIngObj.getString(Food2ForkAPIConstants.TAG_SOURCE_URL),
                            jsonRecipeWoIngObj.getString(Food2ForkAPIConstants.TAG_SOCIAL_RANK)
                            );
                    recipeList.add(recipeWoIngredients);
                }

                recipeSearch = new RecipeSearch(jsonObject.getString(Food2ForkAPIConstants.TAG_COUNT), recipeList);

                Log.d(RecipeSuggestionModel.class.getSimpleName(), "recipeSearch: " + recipeSearch);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("ServiceHandler", "Couldn't get any data from the url");
        }

        return recipeSearch;
    }

    public static Recipe parseGetJsonObjToRecipeObj(String jsonStr){

        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(jsonStr);
        }catch(JSONException e){
            e.printStackTrace();
        }

        Recipe recipe = null;

        if (jsonStr != null) {

            try {

                JSONObject jo = jsonObject.getJSONObject(Food2ForkAPIConstants.TAG_RECIPE);
                JSONArray jsonIngredientArray = jo.getJSONArray(Food2ForkAPIConstants.TAG_INGREDIENTS);
                ArrayList<Ingredient> ingredientList = new ArrayList<Ingredient>();

                for(int i=0; i<jsonIngredientArray.length(); i++) {
                    ingredientList.add(new Ingredient(jsonIngredientArray.get(i).toString()));
                }

                recipe = new Recipe(jo.getString(Food2ForkAPIConstants.TAG_RECIPE_ID),
                        jo.getString(Food2ForkAPIConstants.TAG_TITLE),
                        ingredientList,
                        jo.getString(Food2ForkAPIConstants.TAG_IMAGE_URL),
                        jo.getString(Food2ForkAPIConstants.TAG_SOURCE_URL),
                        jo.getString(Food2ForkAPIConstants.TAG_SOCIAL_RANK));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("ServiceHandler", "Couldn't get any data from the url");
        }

        Log.d(RecipeSuggestionModel.class.getSimpleName(), "recipe: " + recipe);

        return recipe;
    }

}
