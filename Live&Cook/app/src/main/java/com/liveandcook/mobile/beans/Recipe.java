package com.liveandcook.mobile.beans;

import java.util.ArrayList;

/**
 * Created by Android1 on 8/31/2015.
 */
public class Recipe {

    public String recipeId;
    public String title;
    public ArrayList<Ingredient> ingredientList;
    public String imageUrl;
    public String sourceUrl;
    public String socialRank;

    public Recipe(){
        this.ingredientList = new ArrayList<Ingredient>();
    }

    public Recipe(String recipeId, String title, String imageUrl, String sourceUrl, String socialRank) {
        this.recipeId = recipeId;
        this.title = title;
        this.imageUrl = imageUrl;
        this.sourceUrl = sourceUrl;
        this.socialRank = socialRank;
    }

    public Recipe(String recipeId, String title, ArrayList<Ingredient> ingredientList, String imageUrl, String socialRank, String sourceUrl) {
        this.recipeId = recipeId;
        this.title = title;
        this.ingredientList = ingredientList;
        this.imageUrl = imageUrl;
        this.socialRank = socialRank;
        this.sourceUrl = sourceUrl;
    }

    public Recipe(String imageUrl, String title){
        this.imageUrl = imageUrl;
        this.title = title;
    }

    @Override
    public String toString() {
        String ingredientListStr = "";
        int lon = ingredientList.size();

        for(int i=0; i<lon; i++) {

            if(i < (lon-1)) {
                ingredientListStr += ingredientList.get(i) + ",";
            }
        }
        ingredientListStr += ingredientList.get(lon-1);

        return "{"+
                "recipeId:" + recipeId +
                ",title:" + title +
                ",ingredients:[" + ingredientListStr + "]" +
                ",imageUrl:" + imageUrl +
                ",sourceUrl:" + sourceUrl +
                "socialRank:" + socialRank +
        "}";


    }
}
