package com.liveandcook.mobile.mvc.controllers;

import android.content.Context;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;
import android.widget.Toast;

import com.liveandcook.mobile.R;
import com.liveandcook.mobile.activities.RecipeMainActivity;
import com.liveandcook.mobile.fragments.DonationFragment;
import com.liveandcook.mobile.mvc.models.RecipeMainModel;


/**
 * Created by Android1 on 8/31/2015.
 */
public class RecipeMainController implements NavigationView.OnNavigationItemSelectedListener{

    private RecipeMainModel model;
    private RecipeMainActivity screen;
    private Context context;

    public RecipeMainController(RecipeMainModel model, RecipeMainActivity screen, Context context){
        this.model = model;
        this.screen = screen;
        this.context = context;
    }

    public void startControl(){

    }

    // This method will trigger on item Click of navigation menu
    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {

        //Checking if the item is in checked state or not, if not make it in checked state
        if(menuItem.isChecked()) menuItem.setChecked(false);
        else menuItem.setChecked(true);

        //Closing drawer on item click
        screen.getDrawerLayout().closeDrawers();

        //Check to see which item was being clicked and perform appropriate action
        switch (menuItem.getItemId()){
            //Replacing the main content with ContentFragment Which is our Inbox View;
            case R.id.m_opts_btn_my_cook_book:
                Toast.makeText(screen, "My Cook Book Selected", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.m_opts_btn_my_cook_calendar:
                Toast.makeText(screen,"My Cook Calendar Selected",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.m_opts_btn_shooping_list:
                Toast.makeText(screen,"Shooping List Selected",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.m_opts_btn_meal_groceries:
                Toast.makeText(screen,"Meal Groceries Selected",Toast.LENGTH_SHORT).show();
                return true;
            case R.id.m_opts_btn_donate_1_USD:
                addFragment();
                return true;
            default:
                Toast.makeText(screen,"Nothing Selected. Please select one item from the menu.",Toast.LENGTH_SHORT).show();
                return true;

        }
    }

    public void addFragment(){
        DonationFragment fragment = new DonationFragment();
        FragmentTransaction fragmentTransaction = screen.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.a_mainframe_wf, fragment);
        fragmentTransaction.commit();
    }

}
