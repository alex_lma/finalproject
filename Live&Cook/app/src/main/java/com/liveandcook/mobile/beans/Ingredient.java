package com.liveandcook.mobile.beans;

/**
 * Created by Android1 on 8/31/2015.
 */
public class Ingredient {
    public String description;

    public Ingredient(String description){
        this.description = description;
    }

    @Override
    public String toString(){
        return "{description:" + description + "}";
    }
}
