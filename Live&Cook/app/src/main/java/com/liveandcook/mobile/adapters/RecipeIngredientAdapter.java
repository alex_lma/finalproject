package com.liveandcook.mobile.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.liveandcook.mobile.R;
import com.liveandcook.mobile.beans.Ingredient;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by Android1 on 9/6/2015.
 */
public class RecipeIngredientAdapter extends RecyclerView.Adapter<RecipeIngredientAdapter.RecipeIngredientViewHolder>{

    private ArrayList<Ingredient> ingredientList;

    public RecipeIngredientAdapter(){

    }

    public RecipeIngredientAdapter(ArrayList<Ingredient> ingredientList){
        this.ingredientList = ingredientList;
    }

    public ArrayList<Ingredient> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(ArrayList<Ingredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    @Override
    public RecipeIngredientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cv_recipe_ingredient,parent,false);
        RecipeIngredientViewHolder recipeIngVH = new RecipeIngredientViewHolder(view);
        return recipeIngVH;
    }

    @Override
    public void onBindViewHolder(RecipeIngredientViewHolder holder, int position) {
        holder.tvIngresdient.setText(ingredientList.get(position).description);
    }

    @Override
    public int getItemCount() {
        return ingredientList.size();
    }


    public static class RecipeIngredientViewHolder extends RecyclerView.ViewHolder{
        TextView tvIngresdient;

        public RecipeIngredientViewHolder(View itemView) {
            super(itemView);
            this.tvIngresdient = (TextView) itemView.findViewById(R.id.cv_tv_ingredient);
        }
    }
}
