package com.liveandcook.mobile.interfaces;

import com.liveandcook.mobile.beans.RecipeWoIngredients;

/**
 * Created by Android1 on 9/3/2015.
 */
public interface SuggestionCommunicatorInterface {
    public void updateRecipeIngredients(String recipeId);
}
